<?php

/**
 * Implements hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Two Columns with right Header'),
  'category' => t('Bootstrap'),
  'icon' => 'bootstrap_twocol_header.png',
  'theme' => 'bootstrap_twocol_header',
  'admin css' => '../panels-bootstrap-layouts-admin.css',
  'regions' => array(
    'left' => t('Left'),
    'right' => t('Right'),
    'top-right-1' => t('Top-Right-1'),
    'top-right-2' => t('Top-Right-2'),
    'top-right-3' => t('Top-Right-3'),
  ),
);
