<div class="<?php print $classes ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>


  <?php if ($content['left'] || $content['top-right-1'] || $content['top-right-2'] || $content['top-right-3'] || $content['right']): ?>
   
    <div class="row"> <!-- @TODO: Add extra classes -->
      <div class="col-md-3">
        <?php print $content['left']; ?>
      </div>
      
      <div class="col-md-9">
         <div class="col-md-12">
            <div class="col-md-4">
                <?php print $content['top-right-1']; ?>
            </div>

            <div class="col-md-4">
                <?php print $content['top-right-2']; ?>
            </div>

            <div class="col-md-4">
                <?php print $content['top-right-3']; ?>
            </div>
          </div>
            
        

          <?php print $content['right']; ?>
      </div>
      
    </div>
  <?php endif ?>

  
</div>
