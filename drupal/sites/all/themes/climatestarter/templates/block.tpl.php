<?php
/**
 * @file
 * Omega theme implementation to display a block for the FacetAPI Facet Block.
 * FacetAPI Search facets display block
 */
?>
<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?><?php print $attributes; ?>>
  <div class="block-inner clearfix">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h1>2213123<?php print $title_attributes; ?>><?php print $block->subject; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <div<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
  </div>
</<?php print $tag; ?>>