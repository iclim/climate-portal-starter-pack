#Climate Change Portal Starter Pack for Drupal 7.x

This repository contains the files neccessary to create a new Climate Change Portal using Drupal 7.x.

##Files:
* readme.md - this file
* starter-pack.sql - Full Mysql Dump from Drupal database
* drupal (folder) - Full Drupal Install File Structure

All documentation is contained within the [Wiki][https://bitbucket.org/iclim/climate-portal-starter-pack/wiki/Home].